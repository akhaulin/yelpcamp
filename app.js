const express = require("express"),
      app = express(),
      bodyParser = require("body-parser"),
      mongoose = require("mongoose"),
      flash = require("connect-flash"),
      passport = require("passport"),
      LocalStrategy = require("passport-local"),
      methodOverride = require("method-override"),
      passportLocalMongoose = require("passport-local-mongoose"),
      Campground = require("./models/campground"),
      Comment = require("./models/comment"),
      User = require("./models/user"),
      port = process.env.PORT || 3000;
      seedDB = require("./seeds");

//requiring routes
const authRoutes = require("./routes/auth"),
      campgroundRoutes = require("./routes/campgrounds"),
      commentsRoutes = require("./routes/comments");

// seedDB();
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useFindAndModify', false);
mongoose.connect(process.env.DATABASEURL || 'mongodb://localhost:27017/yelp_camp');
// mongoose.connect('mongodb+srv://Dron:Mongo_ecusas54@yelpcamp.hdoum.mongodb.net/YelpCamp?retryWrites=true&w=majority');
// mongoose.connect("mongodb://localhost:27017/yelp_camp");
//DATABASEURL

//Moment js 
app.locals.moment = require('moment');

//PASSPORT CONFIGURATION
app.use(flash());

app.use(require("express-session")({
    secret: "Batman Forever",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(methodOverride("_method"));

app.use(function(req, res, next) {
    res.locals.currentUser = req.user;
    res.locals.error = req.flash("error");
    res.locals.success = req.flash("success");
    next();
});

app.use(authRoutes);
app.use("/campgrounds", campgroundRoutes);
app.use("/campgrounds/:id/comments", commentsRoutes);

app.listen(port, function() {
    console.log("YelpCamp server is running");
});