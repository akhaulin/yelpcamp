const campground = require("../models/campground");

const express = require("express"),
      router = express.Router({mergeParams: true}),
      Campground = require("../models/campground"),
      middleware = require("../middleware");

//INDEX - shows all campgrounds in DB
router.get("/", function(req, res) {
    Campground.find({}, function(err, allCampgrounds) {
        if (err) {
            console.log(err);
        } else {
            res.render("campgrounds/index", {campgrounds: allCampgrounds, page: 'campgrounds'});
        }
    });
});

//CREATE - sents data to create a new campground
router.post("/", middleware.isLoggedIn, function(req, res) {
    var name = req.body.name,
        image = req.body.image,
        price = req.body.price,
        description = req.body.description,
        author = {
            id: req.user._id,
            username: req.user.username
        },
        newCampground = {name: name, image: image, price: price, description: description, author: author};
    //Create a new campground and save it to DB
    Campground.create(newCampground, function(err, newlyCreated) {
        if (err) {
            console.log(err);
        } else {
            console.log(newlyCreated);
            res.redirect("/campgrounds");
        }
    });
});

//NEW - opens page with form
router.get("/new", middleware.isLoggedIn, function(req, res) {
   res.render("campgrounds/new"); 
});

//SHOW - shows more info about one campground
router.get("/:id", function(req, res) {
    //Find the campground with provided ID
    Campground.findById(req.params.id).populate("comments").exec(function(err, foundCampground) {
        if (err || !foundCampground) {
            req.flash("error", "Campground not found");
            res.redirect('back');
        } else {
            res.render("campgrounds/show", {campground: foundCampground});
        }
    });
});

//EDIT Camground Route
router.get("/:id/edit", middleware.checkCampgroundOwnership, function(req, res) {
    Campground.findById(req.params.id, function(err, foundCampground) {
        res.render("campgrounds/edit", {campground: foundCampground});
    });
});

//UPDATE Campground Route
router.put("/:id", middleware.checkCampgroundOwnership, function(req, res) {
    //find and update the correct campground
    Campground.findByIdAndUpdate(req.params.id, req.body.campground, function(err, updatedCampground) {
        if (err) {
            req.flash('error', err.message);
            return res.redirect('back');
        } else {
            res.redirect("/campgrounds/" + campground._id);
        }
    });
});

//DESTROY Campground Route
router.delete("/:id", middleware.checkCampgroundOwnership, function(req, res) {
    Campground.findByIdAndRemove(req.params.id, function(err) {
        if (err) {
            res.redirect('back');
        } else {
            res.redirect("/campgrounds");
        }
    });
});

module.exports = router;